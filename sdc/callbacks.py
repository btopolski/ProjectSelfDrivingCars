from keras.callbacks import Callback


class ReduceTurnWeigthCallback(Callback):

    def __init__(self, rate, starting_epoch=0, **kwargs):
        super().__init__(**kwargs)
        self.starting_epoch = starting_epoch
        self.rate = rate

    def on_epoch_end(self, epoch, logs=None):
        if epoch >= self.starting_epoch and self.model.loss_weights[0] > 1:
            self.model.loss_weights[0] *= 1/(1 + self.rate)
            print('Modified loss weights. Current values: %s' %
                  self.model.loss_weights)
