from colorsys import rgb_to_hls
from cmath import exp, pi
import numpy as np


def rgb_to_hls_np(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]

    C_max = np.amax(rgb, 2)
    C_min = np.amin(rgb, 2)

    C_delta = C_max - C_min
    C_delta_0 = C_delta == 0
    C_delta[C_delta_0] = 1
    C_sum = C_max + C_min
    C_sum[C_delta_0] = 1
    l = C_sum / 2

    s = np.where(
            l < 0.5,
            C_delta / C_sum,
            C_delta / (2 - C_sum)
    )

    s[C_delta_0] = 0

    is_r_max = C_max == r
    is_g_max = C_max == g

    h = np.where(
        is_r_max,
        (g - b) / C_delta,
        np.where(
            is_g_max,
            2 + (b - r) / C_delta,
            4 + (r - g) / C_delta
        )
    )
    h[C_delta_0] = 0

    h = (h % 6) / 6
    return np.dstack((h, l, s))


def rgb_to_complex_hls(rgb):
    hls = rgb_to_hls_np(rgb)
    h, l, s = hls[:, :, 0], hls[:, :, 1], hls[:, :, 2]

    h_c = np.exp(h * 2j * np.pi)
    h_cr, h_ci = h_c.real, h_c.imag
    return np.dstack((h_cr, h_ci, l, s))


def rgb_to_01(img):
    return img / 255


def randomize_hls_lightness(img):
    if img.shape[2] == 3:
        img[:, :, 1] += np.random.normal(loc=0, scale=0.1)
    elif img.shape[2] == 4:
        img[:, :, 2] += np.random.normal(loc=0, scale=0.1)
    return img


def make_preprocessing_fun(func_list):
    def preprocessing_fun(img):
        for f in func_list:
            img = f(img)
        return img

    return preprocessing_fun
