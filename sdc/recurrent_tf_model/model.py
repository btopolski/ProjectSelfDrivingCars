import os
from collections import deque
from functools import wraps

import numpy as np
import tensorflow as tf

from sdc.recurrent_tf_model.network import get_network


def with_graph(func):
    """
    Decorator that runs method from `Model` class in with this model's graph.
    It's purpose is to get rid of extra indentation in almost every method.
    Also I learned how to decorate.
    :param func:
    :return:
    """
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        with self.graph.as_default():
            return func(self, *args, **kwargs)
    return wrapper


class Model(object):

    def __init__(self, train_batch_size=16, train_series_length=15,
                 validation_batch_size=16, validation_series_length=15,
                 dense_1=256, dense_2=256, dropout_rate=0.1, l2_reg=0.001,
                 image_shape=(120, 160), lstm_cells=256, rgb_only=True,
                 optimizer=tf.train.AdamOptimizer(), weights_path=None,
                 load_for_prediction=False, train_loss_shift=3):

        # model params
        self.train_loss_shift = train_loss_shift
        self.weights_path = weights_path
        self.load_for_prediction = load_for_prediction
        self.rgb_only = rgb_only
        self.validation_batch_size = validation_batch_size
        self.validation_series_length = validation_series_length
        self.dense_1 = dense_1
        self.dense_2 = dense_2
        self.dropout_rate = dropout_rate
        self.image_shape = image_shape
        self.lstm_cells = lstm_cells
        self.l2_reg = l2_reg
        self.train_series_length = train_series_length
        self.train_batch_size = train_batch_size
        self.optimizer = optimizer

        # util fields
        self.sess = None
        self.training_model_dict = None
        self.validation_model_dict = None
        self.predict_model_dict = None
        self.saver = None
        self.best_model_saver = None
        self.graph = None
        self.zero_state = None
        self.train_loss_mask = None
        self.validation_loss_mask = None

    def init_model(self):
        """
        Initializes model. Has to be called manually.
        Moved outside __init__ to avoid automatic creation of graph and
        session.
        :return:
        """
        self._create_graph()
        self._create_models()
        self._create_session()
        self._create_savers()
        self._create_loss_masks()
        self._create_zero_state()

    def _create_graph(self):
        """
        Creates tensorflow graph and binds it to this Model instance.
        :return:
        """
        self.graph = tf.Graph()

    @with_graph
    def _create_models(self):
        """
        Creates dictionaries with entry points to all important tensors in a
        model. There are three models with shared weights: this allows to
        perform validation on larger batches than training, and to make
        'production' predictions image by image.
        :return:
        """

        with tf.variable_scope("Model", reuse=None):
            self.training_model_dict = get_network(
                batch_size=self.train_batch_size,
                time_series_length=self.train_series_length,
                rgb_only=self.rgb_only,
                lstm_cells=self.lstm_cells,
                optimizer=self.optimizer,
                dense_1_size=self.dense_1,
                dense_2_size=self.dense_2,
                dropout_rate=self.dropout_rate,
                shape=self.image_shape,
                l2_reg=self.l2_reg
            )
        with tf.variable_scope("Model", reuse=True):
            self.predict_model_dict = get_network(
                batch_size=1,
                time_series_length=1,
                rgb_only=self.rgb_only,
                lstm_cells=self.lstm_cells,
                optimizer=self.optimizer,
                dense_1_size=self.dense_1,
                dense_2_size=self.dense_2,
                dropout_rate=self.dropout_rate,
                shape=self.image_shape,
                l2_reg=self.l2_reg
            )
        with tf.variable_scope("Model", reuse=True):
            self.validation_model_dict = get_network(
                batch_size=self.validation_batch_size,
                time_series_length=self.validation_series_length,
                rgb_only=self.rgb_only,
                lstm_cells=self.lstm_cells,
                optimizer=self.optimizer,
                dense_1_size=self.dense_1,
                dense_2_size=self.dense_2,
                dropout_rate=self.dropout_rate,
                shape=self.image_shape,
                l2_reg=self.l2_reg
            )

    @with_graph
    def _create_session(self):
        """
        Creates tensorflow session. If model is intended for prediction,
        GPU memory limit is set.
        :return:
        """

        init = tf.global_variables_initializer()
        if self.load_for_prediction:
            gpu_options = tf.GPUOptions(
                per_process_gpu_memory_fraction=0.6
            )
            self.sess = tf.Session(
                config=tf.ConfigProto(gpu_options=gpu_options)
            )
        else:
            self.sess = tf.Session()
        self.sess.run(init)

    @with_graph
    def _create_savers(self):
        """
        Creates tensorflow saver.
        :return:
        """

        self.saver = tf.train.Saver()
        self.best_model_saver = tf.train.Saver()

    def _create_zero_state(self):
        self.zero_state = (
            np.zeros(shape=(self.train_batch_size, self.lstm_cells)),
            np.zeros(shape=(self.train_batch_size, self.lstm_cells))
        )

    def _create_loss_masks(self):
        train_loss_mask = np.ones(
            (self.train_batch_size, self.train_series_length, 1)
        )
        valid_loss_mask = np.ones(
            (self.validation_batch_size, self.validation_series_length, 1)
        )
        for i in range(self.train_batch_size):
            train_loss_mask[i] = np.ones(self.train_series_length).reshape(
                (self.train_series_length, 1)
            )
            train_loss_mask[i][0:self.train_loss_shift, :] = 0

        self.train_loss_mask = train_loss_mask
        self.validation_loss_mask = valid_loss_mask
    
    @with_graph
    def save_weights(self, step):
        """
        Saves current weights to file.
        :param step: Current training step (int)
        :return:
        """

        self.saver.save(
                self.sess,
                self.weights_path,
                global_step=step
            )

    @with_graph
    def save_best_weights(self, step):
        self.best_model_saver.save(
            self.sess,
            os.path.join(self.weights_path, 'best_model/'),
            global_step=step
        )

    @with_graph
    def load_weights(self):
        """
        Load weights from file.
        :return:
        """

        self.saver.restore(
                self.sess,
                tf.train.latest_checkpoint(self.weights_path)
            )

    @with_graph
    def predict(self, x, state):
        """
        Run prediction on input matrix
        :param x: np.array containing input images with shape
        (batch_size, time_series_length, image_dim1, image_dim2, channels)
        :param state: tuple with two np.arrays containing initial state for
        lstm layer.
        :return: list [turn, accelerate, brake, state]
        """
        output_t, output_a, output_b, state = self.sess.run(
            [
                self.predict_model_dict['output_turn'],
                self.predict_model_dict['output_accelerate'],
                self.predict_model_dict['output_brake'],
                self.predict_model_dict['state']
            ],
            {
                self.predict_model_dict['input_placeholder']: x,
                self.predict_model_dict['train_phase']: False,
                self.predict_model_dict['initial_state']: state

            }
        )

        return output_t, output_a, output_b, state

    @with_graph
    def _fit_on_batch(self, x, y_turn, y_accelerate, y_brake):
        """

        :param x: np.array containing input images with shape
        (batch_size, time_series_length, image_dim1, image_dim2, channels)
        :param y_turn: np.array containing turn data with shape
        (batch_size, time_series_length, 1)
        :param y_accelerate: np.array containing accelerate data with shape
        (batch_size, time_series_length, 1)
        :param y_brake: np.array containing brake data with shape
        (batch_size, time_series_length, 1)
        :return:
        """

        _, loss = self.sess.run(
            [
                self.training_model_dict['drive_train_step'],
                self.training_model_dict['total_loss']
            ],
            {
                self.training_model_dict['input_placeholder']: x,
                self.training_model_dict['train_phase']: True,
                self.training_model_dict['initial_state']: self.zero_state,
                self.training_model_dict['turn_placeholder']: y_turn,
                self.training_model_dict[
                    'accelerate_placeholder']: y_accelerate,
                self.training_model_dict['loss_mask_placeholder']: self.train_loss_mask,
                self.training_model_dict['brake_placeholder']: y_brake
            }
        )

        return loss

    @with_graph
    def _valid_on_batch(self, x, y_turn, y_accelerate, y_brake):
        """
        :param x: np.array containing input images with shape
        (batch_size, time_series_length, image_dim1, image_dim2, channels)
        :param y_turn: np.array containing turn data with shape
        (batch_size, time_series_length, 1)
        :param y_accelerate: np.array containing accelerate data with shape
        (batch_size, time_series_length, 1)
        :param y_brake: np.array containing brake data with shape
        (batch_size, time_series_length, 1)
        :return:
        """

        loss, turn_mae, acc_mae, brake_mae = self.sess.run(
            [
                self.validation_model_dict['validation_loss'],
                self.validation_model_dict['turn_mae'],
                self.validation_model_dict['accelerate_mae'],
                self.validation_model_dict['brake_mae'],
            ],
            {
                self.validation_model_dict['input_placeholder']: x,
                self.validation_model_dict['train_phase']: False,
                self.validation_model_dict['initial_state']: self.zero_state,
                self.validation_model_dict['turn_placeholder']: y_turn,
                self.validation_model_dict['accelerate_placeholder']:
                    y_accelerate,
                self.validation_model_dict['loss_mask_placeholder']:
                    self.validation_loss_mask,
                self.validation_model_dict['brake_placeholder']: y_brake

            }
        )

        return loss, turn_mae, acc_mae, brake_mae

    @with_graph
    def fit_generator(self, generator, num_steps, num_validation_steps,
                      save_interval, valid_interval):
        """

        :param generator: `DataFeeder` instance
        :param num_steps: number of training steps
        :param num_validation_steps: number of steps in each validation run
        :param save_interval: amount of steps between saving model to file
        :param valid_interval: amount of steps between validation runs
        :return:
        """

        train_losses = deque([np.nan] * 20)
        best_loss = 15

        for i in range(num_steps):

            x, y_turn, y_acc, y_brake, _ = generator.get_time_series_batch(
                batch_size=self.train_batch_size,
                time_series_length=self.train_series_length,
                data_set='train'
            )

            train_loss = self._fit_on_batch(x, y_turn, y_acc, y_brake)
            train_losses.append(train_loss)
            train_losses.popleft()

            if i % valid_interval == 0:

                valid_losses_list = []
                valid_turn_mae_list = []
                valid_acc_mae_list = []
                valid_brake_mae_list = []

                for j in range(num_validation_steps):
                    x_valid, y_turn_valid, y_acc_valid, y_brake_valid, _ = \
                        generator.get_time_series_batch(
                            batch_size=self.validation_batch_size,
                            time_series_length=self.validation_series_length,
                            data_set='validation'
                    )

                    v_loss, v_turn_mae, v_acc_mae, v_brake_mae = \
                        self._valid_on_batch(
                            x_valid, y_turn_valid, y_acc_valid, y_brake_valid
                        )

                    valid_losses_list.append(v_loss)
                    valid_turn_mae_list.append(v_turn_mae)
                    valid_acc_mae_list.append(v_acc_mae)
                    valid_brake_mae_list.append(v_brake_mae)

                mean_train_loss = np.nanmean(train_losses)
                mean_valid_loss = np.mean(valid_losses_list)

                print(
                    "Step: %s, moving average loss: %s, \
                    mean_valid_loss: %s, mean_turn_mae: %s, "
                    "mean_accelerate_mae: %s brake_mae: %s" %
                    (i, mean_train_loss, mean_valid_loss,
                     np.mean(valid_turn_mae_list), np.mean(valid_acc_mae_list),
                     np.mean(valid_brake_mae_list))
                )

                if mean_valid_loss < best_loss:
                    self.save_best_weights(i)
                    best_loss = mean_train_loss

            if i % save_interval == 0:
                self.save_weights(i)





