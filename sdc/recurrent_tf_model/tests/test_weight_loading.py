from datetime import datetime

import numpy as np

from sdc.recurrent_tf_model.model import Model


def test_weight_loading():
    w_path = '/media/bartek/shared_ssd/Projekty/ProjectCarsAI/models/'

    model = Model(weights_path=w_path)

    model.init_model()

    model.save_weights(0)

    model2 = Model(weights_path=w_path)

    model2._create_graph()
    model2._create_models()
    model2._create_session()
    model2._create_savers()

    model2.load_weights()

    test_x = np.random.random(
        (1, 1, model.image_shape[0], model.image_shape[1], 3))

    test_state = (
        np.zeros(shape=(1, model.lstm_cells)),
        np.zeros(shape=(1, model.lstm_cells))
    )

    pairs = zip(
        model.predict(test_x, test_state),
        model2.predict(test_x, test_state)
    )

    start = datetime.now()
    model.predict(test_x, test_state)
    end = datetime.now()
    print(end-start)

    for output_pair in pairs:
        np.testing.assert_equal(*output_pair)


if __name__ == "__main__":
    test_weight_loading()