import tensorflow as tf


def get_convolutional_part(train_phase, batch_size=64, time_series_length=10,
                           shape=(120, 160), rgb_only=True, num_1x1_filters=3):
    if rgb_only:
        inputs_layers = 3
    else:
        inputs_layers = 5

    width = shape[0]
    height = shape[1]

    inputs = tf.placeholder(
        tf.float32,
        shape=[batch_size, time_series_length, width, height, inputs_layers]
    )

    inputs_reshape = tf.reshape(
        inputs,
        [batch_size * time_series_length, width, height, inputs_layers]
    )

    x_normalized = tf.layers.batch_normalization(
        inputs_reshape,
        training=train_phase,
        name='batch_norm1'
    )

    x_normalized = tf.layers.conv2d(
        x_normalized,
        num_1x1_filters,
        (1, 1), (1, 1),
        padding='same'
    )

    with tf.name_scope('conv_group_1'):
        conv_1 = tf.layers.conv2d(
            x_normalized, 64, (3, 3), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        conv_2 = tf.layers.conv2d(
            conv_1, 128, (5, 5), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        pool_1 = tf.layers.max_pooling2d(
            conv_2, (2, 2), 2, name='pool1'
        )

        pool_1 = tf.layers.batch_normalization(pool_1, training=train_phase)

    width = width / 2
    height = height / 2

    with tf.name_scope('conv_group_2'):
        conv_3 = tf.layers.conv2d(
            pool_1, 128, (3, 3), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        conv_4 = tf.layers.conv2d(
            conv_3, 128, (3, 3), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        pool_2 = tf.layers.max_pooling2d(conv_4, (2, 2), 2)
        pool_2 = tf.layers.batch_normalization(pool_2, training=train_phase)

    width = width / 2
    height = height / 2

    with tf.name_scope('conv_group_3'):
        conv_5 = tf.layers.conv2d(
            pool_2, 192, (3, 3), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        conv_6 = tf.layers.conv2d(
            conv_5, 192, (3, 3), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        pool_3 = tf.layers.max_pooling2d(conv_6, (2, 2), 2)
        pool_3 = tf.layers.batch_normalization(pool_3, training=train_phase)
    width = int(width / 2)
    height = int(height / 2)

    with tf.name_scope('conv_group_4'):
        conv_7 = tf.layers.conv2d(
            pool_3, 128, (3, 3), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        conv_8 = tf.layers.conv2d(
            conv_7, 96, (3, 3), (1, 1),
            padding='same', activation=tf.nn.relu
        )

        pool_4 = tf.layers.max_pooling2d(conv_8, (2, 2), 2)
        pool_4 = tf.layers.batch_normalization(pool_4, training=train_phase)

    width = int(width / 2)
    height = int(height / 2)

    reshape = tf.reshape(
        pool_4,
        shape=[batch_size, time_series_length, width * height * 96]
    )

    return inputs, reshape


def get_recurrent_part(train_phase, inputs, lstm_cells=128,
                       dense_1_size=256, dropout_rate=0.5):
    dense = tf.layers.dense(inputs, dense_1_size, activation=tf.nn.relu)
    dense = tf.layers.dropout(dense, rate=dropout_rate, training=train_phase)

    initial_state = tf.contrib.rnn.LSTMStateTuple(
        tf.placeholder(tf.float32, [None, lstm_cells]),
        tf.placeholder(tf.float32, [None, lstm_cells])
    )
    cell = tf.contrib.rnn.BasicLSTMCell(lstm_cells)
    outputs, state = tf.nn.dynamic_rnn(
        cell=cell,
        inputs=dense,
        initial_state=initial_state
    )
    # return tf.concat([outputs, dense], axis=-1), initial_state, state
    return outputs, initial_state, state


def get_outputs(train_phase, inputs, dense_2_size=128, dropout_rate=0.5):
    dense = tf.layers.dense(inputs, dense_2_size, activation=tf.nn.relu)
    dense = tf.layers.dropout(dense, rate=dropout_rate, training=train_phase)

    output_turn = tf.layers.dense(dense, 1, activation=tf.nn.tanh)
    output_accelerate = tf.layers.dense(dense, 1, activation=tf.nn.sigmoid)
    output_brake = tf.layers.dense(dense, 1, activation=tf.nn.sigmoid)
    output_speed = tf.layers.dense(dense, 1, activation=tf.nn.sigmoid)
    return output_turn, output_accelerate, output_brake, output_speed


def get_loss(output_turn, output_accelerate, output_brake,
             output_speed, loss_mask, l2_reg):
    y_turn = tf.placeholder(tf.float32, shape=[None, None, 1])
    y_accelerate = tf.placeholder(tf.float32, shape=[None, None, 1])
    y_brake = tf.placeholder(tf.float32, shape=[None, None, 1])
    y_speed = tf.placeholder(tf.float32, shape=[None, None, 1])

    turn_loss_matrix = tf.pow(y_turn - output_turn, 2)
    # accelerate_loss_matrix = tf.losses.log_loss(
    #     y_accelerate, output_accelerate, reduction=tf.losses.Reduction.NONE
    # )
    # brake_loss_matrix = tf.losses.log_loss(
    #     y_brake, output_brake, reduction=tf.losses.Reduction.NONE
    # )
    accelerate_loss_matrix = tf.pow(y_accelerate - output_accelerate, 2)
    brake_loss_matrix = tf.pow(y_brake - output_brake, 2)
    speed_loss_matrix = tf.pow(y_speed - output_speed, 2)

    turn_loss_matrix = tf.multiply(turn_loss_matrix, loss_mask)
    accelerate_loss_matrix = tf.multiply(accelerate_loss_matrix, loss_mask)
    brake_loss_matrix = tf.multiply(brake_loss_matrix, loss_mask)
    speed_loss_matrix = tf.multiply(speed_loss_matrix, loss_mask)

    turn_loss = tf.reduce_mean(turn_loss_matrix)
    accelerate_loss = tf.reduce_mean(accelerate_loss_matrix)
    brake_loss = tf.reduce_mean(brake_loss_matrix)
    speed_loss = tf.reduce_mean(speed_loss_matrix)

    variables = tf.trainable_variables()
    loss_l2 = tf.add_n([tf.nn.l2_loss(v) for v in variables
                       if 'bias' not in v.name]) * l2_reg

    return y_turn, y_accelerate, y_brake, y_speed, \
        turn_loss, accelerate_loss, brake_loss, speed_loss, loss_l2


def get_metrics(y_turn, y_accelerate, y_brake, y_speed,
                output_turn, output_accelerate, output_brake, output_speed):

    turn_mae = tf.reduce_mean(tf.abs(y_turn - output_turn))
    accelerate_mae = tf.reduce_mean(tf.abs(y_accelerate - output_accelerate))
    brake_mae = tf.reduce_mean(tf.abs(y_brake - output_brake))
    speed_mae = tf.reduce_mean(tf.abs(y_speed - output_speed))

    return turn_mae, accelerate_mae, brake_mae, speed_mae


def get_train_ops(turn_loss, accelerate_loss,
                  brake_loss, speed_loss, reg_loss, optimizer):
    loss_for_drive = turn_loss + accelerate_loss + brake_loss + reg_loss
    loss_for_speed = speed_loss + reg_loss

    drive_train_step = optimizer.minimize(loss_for_drive)
    speed_train_step = optimizer.minimize(loss_for_speed)
    return drive_train_step, speed_train_step


def get_network(batch_size=64, time_series_length=10, shape=(120, 160),
                dropout_rate=0.5, rgb_only=True, num_1x1_filters=3,
                lstm_cells=128, dense_1_size=256, dense_2_size=128,
                l2_reg=0.001,
                optimizer=tf.train.AdamOptimizer(learning_rate=0.001)):

    train_phase = tf.placeholder(tf.bool, shape=[])
    loss_mask_placeholder = tf.placeholder(tf.float32, shape=[None, None, 1])

    input_placeholder, conv_output = get_convolutional_part(
        train_phase, batch_size, time_series_length,
        shape, rgb_only, num_1x1_filters)

    recurrent_output, initial_state, state = get_recurrent_part(
        train_phase, conv_output, lstm_cells, dense_1_size, dropout_rate)

    output_turn, output_accelerate, output_brake, output_speed = get_outputs(
        train_phase, recurrent_output, dense_2_size, dropout_rate
    )

    (
        turn_placeholder, accelerate_placeholder, brake_placeholder,
        speed_placeholder, turn_loss, accelerate_loss, brake_loss, speed_loss,
        reg_loss
    ) = get_loss(
        output_turn, output_accelerate, output_brake,
        output_speed, loss_mask_placeholder, l2_reg
    )

    turn_mae, accelerate_mae, brake_mae, speed_mae = get_metrics(
        turn_placeholder, accelerate_placeholder, brake_placeholder,
        speed_placeholder, output_turn, output_accelerate, output_brake, output_speed
    )

    drive_train_step, speed_train_step = get_train_ops(
        turn_loss, accelerate_loss, brake_loss, speed_loss, reg_loss, optimizer
    )

    total_loss = turn_loss + accelerate_loss + brake_loss + reg_loss
    validation_loss = turn_loss + accelerate_loss + brake_loss
    return {
        'train_phase': train_phase,
        'input_placeholder': input_placeholder,
        'output_turn': output_turn,
        'output_accelerate': output_accelerate,
        'output_brake': output_brake,
        'output_speed': output_speed,
        'turn_placeholder': turn_placeholder,
        'accelerate_placeholder': accelerate_placeholder,
        'brake_placeholder': brake_placeholder,
        'speed_placeholder': speed_placeholder,
        'turn_loss': turn_loss,
        'accelerate_loss': accelerate_loss,
        'brake_loss': brake_loss,
        'speed_loss': speed_loss,
        'total_loss': total_loss,
        'validation_loss': validation_loss,
        'drive_train_step': drive_train_step,
        'speed_train_step': speed_train_step,
        'initial_state': initial_state,
        'state': state,
        'loss_mask_placeholder': loss_mask_placeholder,
        'turn_mae': turn_mae,
        'accelerate_mae': accelerate_mae,
        'brake_mae': brake_mae,
        'speed_mae': speed_mae
    }
