from sdc.loader import FromDiskDataFeeder
import os
import unittest
from sdc.preprocessing import (
    rgb_to_hls_np,
    rgb_to_complex_hls,
    rgb_to_01,
    make_preprocessing_fun,
    randomize_hls_lightness
)
from colorsys import rgb_to_hls
import numpy as np
from time import time
import yaml


class TestPreprocessing(unittest.TestCase):
    def test_my_hsl(self):
        img = np.random.random(size=(160, 160, 3))

        normal_hsl = np.apply_along_axis(
            lambda x: rgb_to_hls(*x),
            2,
            img
        )


        my_hsl = rgb_to_hls_np(img)
        np.testing.assert_almost_equal(normal_hsl, my_hsl)

        start = time()
        for _ in range(16*5):
            img = np.random.random(size=(160, 160, 3))
            my_hsl = rgb_to_complex_hls(img)
        end = time()
        print((end-start))


    def test_preprocessing_fun(self):

        pixel = (25, 36, 121)

        pixel_01 = rgb_to_01(pixel)
        pixel_hls = rgb_to_complex_hls(pixel_01)

        functions = [rgb_to_01, rgb_to_complex_hls]
        preprocessing_fun = make_preprocessing_fun(functions)

        result_combined = preprocessing_fun(pixel)

        assert pixel_hls == result_combined


    def test_loading_time(self):
        with open("config.yml", 'r') as ymlfile:
            cfg = yaml.load(ymlfile)

        paths = glob(os.path.join(cfg['DATA']['MAIN_PATH'], '*'))

        preprocessing_fun = make_preprocessing_fun(
            [rgb_to_01, rgb_to_complex_hls, randomize_hls_lightness]
        )
        data_feeder = FromDiskDataFeeder(paths)

        data_feeder.initialize_data()
        data_feeder.split_data()
        generator = data_feeder.speed_data_generator(
            'train', batch_size=16, time_series_length=5,
            preprocessing_fn=preprocessing_fun
        )
        print(data_feeder.max_speed)

        start = time()
        for _ in range(10):
            imgs, y_speed = \
                next(generator)
            print(np.mean(imgs))
        end = time()
        print((end-start)/10)
        print(imgs[0][0].shape)