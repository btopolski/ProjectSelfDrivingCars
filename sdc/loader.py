import numpy as np
import pandas as pd
import cv2
from glob import glob
import scipy.misc
import os


def get_single_file(path, size=(160, 120), rgb_only=True):
    file = np.load(path)
    if rgb_only:
        file = file[:, :, :3]
    image = cv2.resize(file, size)
    return image


def get_single_file_png(path, size=(160, 120), preprocessing_fn=None):
    image = scipy.misc.imread(path)
    image = scipy.misc.imresize(image, size)
    if preprocessing_fn:
        return preprocessing_fn(image)
    return image


class FromMemoryDataFeeder(object):
    def __init__(self, dirs, rgb_only=True):
        self.rgb_only = rgb_only
        self.dirs = dirs
        self.data = {}
        self.y_df = None
        self.max_speed = None
        self.tracks = None
    
    def load_data(self, size=(160, 120), limit_files=None):
        for path in self.dirs:
            track = path.split("/")[-2]
            images_paths = sorted(
                glob(
                    os.path.join(path, "images/*.npy")
                )
            )
            file_names = []
            if limit_files:
                images_paths = images_paths[:limit_files]
            for im_path in images_paths:
                file_name = im_path.split('/')[-1].split('.')[0]
                self.data[file_name] = get_single_file(
                    im_path, size=size, rgb_only=self.rgb_only
                )
                if limit_files:
                    file_names.append(file_name)
                
            label_file = pd.read_csv(
                os.path.join(path, "labels.txt"), index_col=0
            )
            label_file['track'] = track  
            if limit_files:
                label_file = label_file.loc[file_names]
             
            if self.y_df is None:                
                self.y_df = label_file
            else:
                self.y_df = pd.concat([self.y_df, label_file], axis=0)             
        self.y_df['dataset'] = 'train'
        self.max_speed = self.y_df['carState_mSpeed'].max()
        self.y_df['carState_mSpeed'] = self.y_df['carState_mSpeed']/self.max_speed
        self.tracks = self.y_df['track'].unique()
    
    def get_batch(self, dataset, batch_size=32):
        indices = self.y_df[self.y_df['dataset'] == dataset].index.to_series().sample(n=batch_size)
        X_list = [self.data[i] for i in indices]
        X_array = np.stack(X_list)
        y_turn = self.y_df.loc[indices, 'carState_mSteering']
        y_accelerate = self.y_df.loc[indices, 'carState_mThrottle']
        y_brake = self.y_df.loc[indices, 'carState_mBrake']
        X_speed = self.y_df.loc[indices, 'carState_mSpeed']
        return X_array, y_turn, y_accelerate, y_brake, X_speed
    
    def split_data(self, validation_ratio=0.2, test_ratio=0.2):
        for track in self.tracks:
            data = self.y_df[self.y_df['track'] == track]
            train_ratio = 1 - validation_ratio - test_ratio
            split_1, split_2 = \
                int(data.shape[0]*train_ratio), \
                int(data.shape[0]*(train_ratio + validation_ratio))
            data['dataset'].iloc[split_1:split_2] = 'validation'
            data['dataset'].iloc[split_2:] = 'test'
            self.y_df[self.y_df['track'] == track] = data
        
    def get_time_series_batch(self, data_set,
                              batch_size=64, time_series_length=10, step=1):
        tracks = self.tracks
        batch_list = []
        y_turn_list = []
        y_accelerate_list = []
        y_brake_list = []
        x_speed_list = []
        for i in range(batch_size):
            track = np.random.choice(tracks)
            indices = self.y_df[(self.y_df['dataset'] == data_set) & (self.y_df['track'] == track)].index
            max_index = len(indices) - step*time_series_length
            start_index = np.random.choice(range(max_index))            
            end_index = start_index + step*time_series_length
            selected_indices = indices[start_index:end_index:step]
            last_index = selected_indices[-1]
            y_turn = self.y_df.loc[last_index, 'carState_mSteering']
            y_accelerate = self.y_df.loc[last_index, 'carState_mThrottle']
            y_brake = self.y_df.loc[last_index, 'carState_mBrake']
            X_speed = self.y_df.loc[last_index, 'carState_mSpeed']
            data = [self.data[i] for i in selected_indices]
            batch_list.append(data)
            y_turn_list.append(y_turn)
            y_accelerate_list.append(y_accelerate)
            y_brake_list.append(y_brake)
            x_speed_list.append(X_speed)

        return (batch_list,
                np.array(y_turn_list),
                np.array(y_accelerate_list),
                np.array(y_brake_list),
                np.array(x_speed_list))

    def speed_data_generator(self, data_set,
                        batch_size=64, time_series_length=10, step=1):
        while True:
            images, _, _, _, speed = self.get_time_series_batch(
                data_set, batch_size, time_series_length, step)
            images = [
                np.array(images)[:, i, :, :, :]
                for i in range(time_series_length)
            ]
            yield images, speed
    
            
class FromDiskDataFeeder(object):
    def __init__(self, dirs):
        self.dirs = dirs
        self.y_dfs = {}
        self.max_speed = 0

        self.dir_dict = {path.split("/")[-1]: path for path in self.dirs}
        self.tracks = list(self.dir_dict.keys())

    def initialize_data(self):
        for track in self.dir_dict:
            path = self.dir_dict[track]
            label_file = pd.read_csv(
                os.path.join(path, "labels.txt"), index_col=0
            )
            self.y_dfs[track] = {
                'train': label_file
            }
            self.max_speed = max(
                self.max_speed,
                label_file['carState_mSpeed'].max()
            )

    def split_data(self, validation_ratio=0.2, test_ratio=0.2):
        for track in self.tracks:
            data = self.y_dfs[track]['train']
            train_ratio = 1 - validation_ratio - test_ratio
            split_1, split_2 = \
                int(data.shape[0]*train_ratio), \
                int(data.shape[0]*(train_ratio + validation_ratio))

            self.y_dfs[track]['valid'] = data.iloc[split_1:split_2]
            self.y_dfs[track]['test'] = data.iloc[split_2:]
            self.y_dfs[track]['train'] = data.iloc[:split_1]

    def get_time_series_batch_filenames(self, data_set,
                              batch_size=64, time_series_length=10, step=1):
        tracks = self.tracks
        batch_list = []
        y_turn_list = []
        y_accelerate_list = []
        y_brake_list = []
        x_speed_list = []
        for i in range(batch_size):
            track = np.random.choice(tracks)
            data = self.y_dfs[track][data_set]
            indices = data.index
            max_index = len(indices) - step*time_series_length
            start_index = np.random.choice(range(max_index))
            end_index = start_index + step*time_series_length
            selected_indices = indices[start_index:end_index:step]
            last_index = selected_indices[-1]
            y_turn = data.loc[last_index, 'carState_mSteering']
            y_accelerate = data.loc[last_index, 'carState_mThrottle']
            y_brake = data.loc[last_index, 'carState_mBrake']
            X_speed = data.loc[last_index, 'carState_mSpeed']

            paths = [
                os.path.join(self.dir_dict[track], 'images', idx + '.png')
                for idx in selected_indices
            ]

            batch_list.append(paths)
            y_turn_list.append(y_turn)
            y_accelerate_list.append(y_accelerate)
            y_brake_list.append(y_brake)
            x_speed_list.append(X_speed)

        # now, reorder list
        batch_list = [
            [l[i] for l in batch_list]
            for i in range(time_series_length)
        ]

        return (batch_list,
                np.array(y_turn_list),
                np.array(y_accelerate_list),
                np.array(y_brake_list),
                np.array(x_speed_list)
                )

    def speed_data_generator(self, data_set, file_size=(160, 160),
                             batch_size=64, time_series_length=10, step=1,
                             preprocessing_fn=None):
        while True:
            images_paths, _, _, _, speed = self.get_time_series_batch_filenames(
                data_set, batch_size, time_series_length, step)

            images = [
                np.array([get_single_file_png(
                    path, file_size, preprocessing_fn
                ) for path in l])
                for l in images_paths
            ]

            yield images, speed/self.max_speed

    def drive_data_generator(self, data_set, file_size=(160, 160),
                             batch_size=64, time_series_length=10, step=1,
                             preprocessing_fn=None):
        while True:
            images_paths, turn, acc, brake, speed = self.get_time_series_batch_filenames(
                data_set, batch_size, time_series_length, step)

            images = [
                np.array([get_single_file_png(
                    path, file_size, preprocessing_fn
                ) for path in l])
                for l in images_paths
            ]

            yield images, [turn, acc, brake, speed/self.max_speed]
