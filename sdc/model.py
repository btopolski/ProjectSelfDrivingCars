from keras.layers import Input, Dense, Conv2D, \
    BatchNormalization, Dropout, MaxPooling2D, Flatten, Concatenate
from keras.models import Model
from keras.activations import relu, linear, sigmoid, tanh
import keras.backend as K

def create_thresholded_relu(max_value=1):
    def thresholded_relu(x):
        return K.relu(x, max_value=max_value)
    return thresholded_relu


def get_speed_model(image_shape, num_inputs, num_input_channels=3):

    dim1, dim2 = image_shape

    input_shape = (dim1, dim2, num_input_channels)
    image_input = Input(shape=input_shape)
    x_extractor = Conv2D(
        filters=64, kernel_size=(3, 3)
    )(image_input)
    x_extractor = Conv2D(
        filters=64, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(2, 2))(x_extractor)
    x_extractor = BatchNormalization()(x_extractor)
    x_extractor = Conv2D(
        filters=128, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(2, 2))(x_extractor)
    x_extractor = BatchNormalization()(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(4, 4))(x_extractor)
    x_extractor = BatchNormalization()(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = Conv2D(
        filters=128, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(4, 4))(x_extractor)
    x_extractor = Flatten()(x_extractor)

    image_feature_extractor = Model(inputs=image_input, outputs=x_extractor)
    inputs = [Input(shape=input_shape) for _ in range(num_inputs)]
    image_features = [image_feature_extractor(input_) for input_ in inputs]

    concat = Concatenate()(image_features)

    x = Dense(512, activation=relu)(concat)
    x = Dropout(0.2)(x)
    x = Dense(256, activation=relu)(x)
    x = Dropout(0.2)(x)
    out = Dense(1, activation=create_thresholded_relu(2))(x)

    speed_model = Model(inputs=inputs, outputs=out)

    return speed_model


def get_driving_model(image_shape, num_inputs, num_input_channels=3):
    dim1, dim2 = image_shape

    input_shape = (dim1, dim2, num_input_channels)
    image_input = Input(shape=input_shape)
    x_extractor = Conv2D(
        filters=64, kernel_size=(3, 3)
    )(image_input)
    x_extractor = Conv2D(
        filters=64, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(2, 2))(x_extractor)
    x_extractor = BatchNormalization()(x_extractor)
    x_extractor = Conv2D(
        filters=128, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(2, 2))(x_extractor)
    x_extractor = BatchNormalization()(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(4, 4))(x_extractor)
    x_extractor = BatchNormalization()(x_extractor)
    x_extractor = Conv2D(
        filters=256, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = Conv2D(
        filters=128, kernel_size=(3, 3)
    )(x_extractor)
    x_extractor = MaxPooling2D(strides=(4, 4))(x_extractor)
    x_extractor = Flatten()(x_extractor)

    image_feature_extractor = Model(inputs=image_input, outputs=x_extractor)
    inputs = [Input(shape=input_shape) for _ in range(num_inputs)]
    image_features = [image_feature_extractor(input_) for input_ in inputs]

    concat = Concatenate()(image_features)

    x = Dense(512, activation=relu)(concat)
    x = Dropout(0.2)(x)
    x = Dense(512, activation=relu)(x)
    x = Dropout(0.2)(x)
    turn_out = Dense(1, activation=tanh)(x)
    acc_out = Dense(1, activation=sigmoid)(x)
    brake_out = Dense(1, activation=sigmoid)(x)
    speed_out = Dense(1, activation=create_thresholded_relu(2))(x)

    model = Model(inputs=inputs,
                  outputs=[turn_out, acc_out, brake_out, speed_out])

    return model


