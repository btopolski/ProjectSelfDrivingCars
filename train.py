from sdc.loader import  FromDiskDataFeeder
from sdc.model import get_speed_model, get_driving_model
from sdc.preprocessing import \
    rgb_to_01, rgb_to_complex_hls, make_preprocessing_fun, \
    randomize_hls_lightness
from sdc.callbacks import ReduceTurnWeigthCallback
import os
from glob import glob
import yaml
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard
import datetime


def train_speed():
    with open("config.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    paths = glob(os.path.join(cfg['DATA']['MAIN_PATH'], '*'))

    model_dir = os.path.join(
        cfg['DATA']['MODEL_OUTPUT_PATH'],
        datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    )
    os.makedirs(model_dir, exist_ok=True)
    model_name = os.path.join(model_dir, 'model.h5')
    model_path = os.path.join(cfg['DATA']['MODEL_OUTPUT_PATH'], model_name)

    preprocessing_fun_train = make_preprocessing_fun(
        [rgb_to_01, rgb_to_complex_hls, randomize_hls_lightness]
    )

    preprocessing_fun_validation = make_preprocessing_fun(
        [rgb_to_01, rgb_to_complex_hls]
    )

    data_feeder = FromDiskDataFeeder(paths)

    data_feeder.initialize_data()
    data_feeder.split_data()

    train_generator = data_feeder.speed_data_generator(
        'train', batch_size=16, time_series_length=5, step=1,
        preprocessing_fn=preprocessing_fun_train, file_size=(160, 160))

    validation_generator = data_feeder.speed_data_generator(
        'valid', batch_size=16, time_series_length=5, step=1,
        preprocessing_fn=preprocessing_fun_validation, file_size=(160, 160))

    test_generator = data_feeder.speed_data_generator(
        'test', batch_size=32, time_series_length=5, step=1,
        preprocessing_fn=preprocessing_fun_validation, file_size=(160, 160))

    model = get_speed_model((160, 160), 5, 4)

    print(model.summary())
    print(data_feeder.max_speed)

    model.compile(loss='mse', optimizer='adam', metrics=['mae'])

    model.fit_generator(
        generator=train_generator,
        validation_data=validation_generator,
        validation_steps=30,
        steps_per_epoch=300,
        epochs=60,
        callbacks=[
            EarlyStopping(min_delta=0.0005, patience=5, verbose=0),
            ModelCheckpoint(filepath=model_path, save_best_only=True)
        ],
        verbose=1,
        use_multiprocessing=True,
        workers=6
    )

    model.load_weights(model_path)

    eval_metrics = model.evaluate_generator(
        test_generator,
        steps=200,
        use_multiprocessing=True,
        workers=6
    )

    print(model.metrics_names)
    print(eval_metrics)


def train_driving():
    with open("config.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    paths = glob(os.path.join(cfg['DATA']['MAIN_PATH'], '*'))

    model_dir = os.path.join(
        cfg['DATA']['MODEL_OUTPUT_PATH'],
        datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    )
    os.makedirs(model_dir, exist_ok=True)
    model_name = os.path.join(model_dir, 'model.h5')
    model_path = os.path.join(cfg['DATA']['MODEL_OUTPUT_PATH'], model_name)

    log_path = os.path.join(
        cfg['DATA']['MODEL_LOG_PATH'],
        datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    )
    os.makedirs(log_path, exist_ok=True)

    preprocessing_fun_train = make_preprocessing_fun(
        [rgb_to_01, rgb_to_complex_hls, randomize_hls_lightness]
    )

    preprocessing_fun_validation = make_preprocessing_fun(
        [rgb_to_01, rgb_to_complex_hls]
    )

    data_feeder = FromDiskDataFeeder(paths)

    data_feeder.initialize_data()
    data_feeder.split_data()

    train_generator = data_feeder.drive_data_generator(
        'train', batch_size=16, time_series_length=5, step=1,
        preprocessing_fn=preprocessing_fun_train, file_size=(160, 160))

    validation_generator = data_feeder.drive_data_generator(
        'valid', batch_size=16, time_series_length=5, step=1,
        preprocessing_fn=preprocessing_fun_validation, file_size=(160, 160))

    test_generator = data_feeder.drive_data_generator(
        'test', batch_size=32, time_series_length=5, step=1,
        preprocessing_fn=preprocessing_fun_validation, file_size=(160, 160))

    model = get_driving_model((160, 160), 5, 4)

    print(model.summary())
    print(data_feeder.max_speed)

    model.compile(loss='mse', optimizer='adam', metrics=['mae'],
                  loss_weights=[10, 3, 1, 1])

    model.fit_generator(
        generator=train_generator,
        validation_data=validation_generator,
        validation_steps=30,
        steps_per_epoch=300,
        epochs=240,
        callbacks=[
            EarlyStopping(min_delta=0.0005, patience=20, verbose=0),
            ModelCheckpoint(filepath=model_path, save_best_only=True),
            ReduceTurnWeigthCallback(0.04, 3),
            TensorBoard(log_dir=log_path)
        ],
        verbose=1,
        use_multiprocessing=True,
        workers=6
    )

    model.load_weights(model_path)

    eval_metrics = model.evaluate_generator(
        test_generator,
        steps=200,
        use_multiprocessing=True,
        workers=6
    )

    print(model.metrics_names)
    print(eval_metrics)


if __name__ == '__main__':
    train_driving()


